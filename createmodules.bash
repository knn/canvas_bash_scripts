#!/bin/bash -xv

#(c) Kent Gruber (ignore this because it's to amuse myself)

#CANBASH: Create modules from list

clear

echo "

 __         __      __   
/   /\ |\ ||__) /\ (_ |__|
\__/--\| \||__)/--\__)|  | v1 - MODULES"

echo "[ + ] Create modules for given course..."

echo "[ * ] Optionally keeps the data generated (pretty) json format in a .txt document"

read -p "[ ? ] COURSE ID: " COURSE

read -p "[ ? ] LIST: " LIST

 

# hardcoded auth token:

#store working directory for fun

canPWD=${PWD}

 

# CREATE MODULES, PARSE DATA, STORE, SEARCH, LOG IT.


echo "[ + ] Creating modules ...  "

echo

IFS=$'\n'


for i in $(cat $canPWD/$LIST.txt) ;

do

echo "Module :->  $i ..."

# DO THE CURLING

curl -# -o $i\_modules$COURSE.txt https://eaaa.instructure.com/api/v1/courses/$COURSE/modules \
     -X POST \
     -d "module[name]=$i" \
     -H 'Authorization: Bearer 10913~mP17JV5OAjfUt98leNwplBGyAG8ABZEbrhWcKdpjG6kFWnKq4bzOT943eUUinKQ7'

cat $i\_modules$COURSE.txt | python -m json.tool >> modules$COURSE.txt

cat $i\_modules$COURSE.txt | python -m json.tool >> $i\_modules$COURSE.txt

#turn off for some flooding action. Will mess with position calls.

sleep 1

done

echo

read -p "[ ? ] Enter the search pattern (email, id, name...):" pattern

echo -e "$(sed '1d' modules$COURSE.txt)\n" > modules$COURSE.txt

echo -e "$(sed 's|["{},]||g' modules$COURSE.txt)\n" > modules$COURSE.txt

if [ -f "modules$COURSE.txt" ]

then

    result=$(grep -i "$pattern" "modules$COURSE.txt")

    echo "$result"

fi

 

echo "[ ? ] Keep generated files [Y,n]"

read input

if [[ $input == "Y" || $input == "y" ]]; then

        clear

        touch id.txt

        grep -i "id:" modules$COURSE.txt | awk '{print $2}' >> id.txt

        cat $LIST.txt > mods.txt

        paste id.txt mods.txt > report.txt

        echo "[ + ] Kept"

        mkdir $COURSE

        mv *.txt $COURSE

else

        clear

        for i in $(cat $canPWD/$LIST.txt) ; do rm $i\_modules$COURSE.txt ; done

        rm modules$COURSE.txt

        echo "[ - ] Removed"

fi